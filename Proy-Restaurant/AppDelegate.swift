//
//  AppDelegate.swift
//  Proy-Restaurant
//
//  Created by Victor Hugo Benitez Bosques on 04/02/17.
//  Copyright © 2017 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("Cargar variables, base de datos y datos iniciales didFinishLaunchingWithOptions")
        
        // INICIAR LA LIBRERIA DataBase()
        DataBase.checkAndCreateDatabase()  // Copia la BD Dish.sqlite para ser ocupada de manera interna por la aplicacion

        return true
    }
    
//    func copyDBtoDirectory(nameBD : String){
//        
//        let objectFileManager = FileManager.default
//        let objectFileHelper = FileHelper()
//        let pathDBDocumentos = objectFileHelper.pathArchivoEnCarpetaDocumentos(nombreArchivo: nameBD)
//        let pathDBBundle = objectFileHelper.pathBaseDatosEnBundle(nombreBaseDatos: nameBD)
//        
//        // pasar el archivo BD al path de carpeta documentos
//        if objectFileHelper.existeArchivoEnDocumentos(nombreArchivo: nameBD){
//            
//            print("Ya tengo la DB en la carpeta de documentos")
//            
//        }else{
//            
//            do {
//                try objectFileManager.copyItem(atPath: pathDBBundle, toPath: pathDBDocumentos)
//            } catch let Error {
//                print("Error al tratar de copiar el archivo BD al de documentos ", Error.localizedDescription)
//            }
//            
//            
//        }
//        
//        
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        print("Va a entrar en modo inactivo modo estado background applicationWillResignActive")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Cuando ya entro en modo background applicationDidEnterBackground")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        print("Cuando se van a mostrar las diferentes pantalla de nuestra aplicacion applicationWillEnterForeground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        print("applicationDidBecomeActive")
        // fetch values from local server
        parseAndSincronizar()  // delete the data base and insert the records in the Restaurant.sqlite
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    
    
    
    func parseAndSincronizar(){
        
        let objDAO = DataBase()
        
        // Corectly parsing
        let urlString = "http://localhost/swiftAppRestaurant/web-service/listado.php"
        let url = URL(string: urlString)
        
        // 1. fetch a JSON response and store the results , request
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            
            if error != nil {
                print("\(error)")
                
            } else {
                
                // 2. backend respond and parser JSON
                do {
                    
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String : Any]]
                    
                    // 3. Delete the records from the DB table platillos
                    objDAO.ejecutarDelete("delete from platillos")
                    
                    // 4. Insert the JSON objects from the local server to DB table platillos
                    if let datos = parsedData{
                        
                        for item in datos{
                            
                            let nombrePlatillo = item["nombre_platillo"] as! String
                            let precioPlatillo = item["precio_platillo"] as! String
                            let descripcionPlatillo = item["descripcion_platillo"] as! String
                            let imagePlatillo = item["imagen_platillo"] as! String
                            
                            // Tanto la BD de sqlite y local no tiene el campo categoria
                            
                            // 4.1 Insert the record in the Data base Restaurant.sqlite
                            objDAO.ejecutarInsert("insert into platillos('nombre_platillo', 'precio_platillo', 'descipcion_platillo', 'imagen_platillo') values('\(nombrePlatillo)', '\(precioPlatillo)','\(descripcionPlatillo)','\(imagePlatillo)')")
                            
                        }
                        
                    }
                    
                } catch let error as NSError {  // else throw an error detailing what went wrong
                    print(error)
                }
            }
            
            }.resume()
    }
    


}

