//
//  DetailViewController.swift
//  Proy-Restaurant
//
//  Created by Victor Hugo Benitez Bosques on 05/02/17.
//  Copyright © 2017 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var dishimageView: UIImageView!
    @IBOutlet var nameDishLabel: UILabel!
    @IBOutlet var priceDishLabel: UILabel!
    @IBOutlet var desciptionLabel: UITextView!
    
    
    var dish = [String : Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       
        
        if let nameDish = dish["precio_platillo"], let priceDish = dish["precio_platillo"], let description = dish["descipcion_platillo"]{
            self.nameDishLabel.text = "\(nameDish)"
            self.priceDishLabel.text = "\(priceDish)"
            self.desciptionLabel.text = "\(description)"
        }
        
        if let imageName = dish["imagen_platillo"] as? String{
            self.dishimageView.image = UIImage(named: imageName)
        }

        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func ordenButtonPressed(_ sender: Any) {
        
        
    }
    

}
