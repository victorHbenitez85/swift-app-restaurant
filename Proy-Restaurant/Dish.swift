//
//  Dish.swift
//  Proy-Restaurant
//
//  Created by Victor Hugo Benitez Bosques on 05/02/17.
//  Copyright © 2017 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class Dish {
    //var nameDictionary : Dictionary<String, Any>!
    var nameKey : String!
    var nameDish : String!
    var description : String!
    var price : String!
    var nameImage : String!
    
    init(nameKey : String, name : String, description : String, price : String, nameImage : String) {
        self.nameKey = nameKey
        self.nameDish = name
        self.description = description
        self.price = price
        self.nameImage = nameImage
    }
    
}


