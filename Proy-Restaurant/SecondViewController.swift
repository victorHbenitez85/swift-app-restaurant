//
//  SecondViewController.swift
//  Proy-Restaurant
//
//  Created by Victor Hugo Benitez Bosques on 04/02/17.
//  Copyright © 2017 Victor Hugo Benitez Bosques. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    // Data model : These Collection will be data for the table view Cells
    // var recipeDishes = [Dish]()  // SE USO PARA LA CLASE Dish.swift coleccion de datos no se esta usando hasta este punto
    var arrayDish = [[String : Any]]()  // array de diccionarios
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Funcion del ciclo de vida de una vista viewDidLoad")
        
        // AL MODIFICAR O MAINUPLAR LA BD DE SQLITE SE PIERDE CONEXION POR PARTE DEL DAO ESO CREO
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableView.delegate = self
        tableView.dataSource = self
    
              
        
    }
    
        
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        print("Funcion del ciclo de vida de una vista viewWillAppear")
        
              
        // INSTANCIA DE DataBase
        let objDAO = DataBase()
        
        // Fetch : go and get values from Restaurant.sqlite Casting : array dictionaries
        arrayDish = objDAO.ejecutarSelect("select * from platillos") as! [[String : Any]]
        
//        print("Datos obtenidos desde la bd Restaurant.sqlite y viewWillAppear", arrayDish )
        
        //Reloads the rows and sections of the table view.
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        print("Funcion del ciclo de vida de una vista viewDidAppear")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        print("Funcion del ciclo de vida de una vista viewWillDisappear")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print("Funcion del ciclo de vida de una vista viewDidDisappear")
        
    }
    
}

// MARK: - UITableViewDelegate
extension SecondViewController : UITableViewDelegate{
    
    // Number of section in table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Number of row in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.arrayDish.count)
        return self.arrayDish.count  // number of rows arrayDish : [[String : Any]]
        
    }

    
    // Create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Cell Reuse id (cells that scroll out of view can be reused)
        let cellReuseIdentifier = "ReuseIdentifier"
        
        // Para ser uso de RecipeCell casting a la Celda Editada
        let  cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! DishCell //Le inicamos sobre la celda que usara los datos

        
        // get the dish from collection
        // posicion del array
        
        // Set the text from the date model
        if let nombre = arrayDish[indexPath.row]["nombre_platillo"]{
            cell.nameLabel.text = "\(nombre)" //Get valor del diccionario
        }
        if let precio = arrayDish[indexPath.row]["precio_platillo"]{
            cell.priceLabel.text = "Precio: \(precio)"
        }
        
        if let imageName = arrayDish[indexPath.row]["imagen_platillo"] as? String{
            cell.thumbnalImageView.image = UIImage(named: imageName)
        }
        return cell
        
    }
    
    // Method to run when table view cell is tapped, select my favorite Dish
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print("You tapped cell number \(indexPath.row).")
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDishDetail"{  //identificamos el segue por donde se enviara los datos
            
            if let indexPath = self.tableView.indexPathForSelectedRow{
                
                let selectedDish = self.arrayDish[indexPath.row]
                let destinatedViewController = segue.destination as! DetailViewController  //viewController destino acceso a sus func y atributos
                destinatedViewController.dish = selectedDish  // Dato que enviamos
                
            }
        }
    }

    
    
  
}

// MARK: - IUTableViewDataSource
extension SecondViewController : UITableViewDataSource{

}




